import 'package:flutter_driver/driver_extension.dart';
import 'package:heresmycard_frontend/main.dart' as app;

void main() {
  enableFlutterDriverExtension();

  app.main();
}