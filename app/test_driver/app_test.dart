import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('Registration Process', () {
    // First, define the Finders and use them to locate widgets from the
    // test suite. Note: the Strings provided to the `byValueKey` method must
    // be the same as the Strings we used for the Keys in step 1.
    final idTextFinder = find.byValueKey('registration_id_field');
    final pwTextFinder = find.byValueKey('registration_pw_field');
    final buttonFinder = find.byValueKey('registration_submit_button');

    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('has label "Next"', () async {
      //expect(await driver.getText(buttonFinder), "Next");
       await driver.waitFor(find.text("Next"));
    });
  });
}