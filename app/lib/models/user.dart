import 'dart:convert';
import 'package:heresmycard_frontend/connectors/backend.dart';

import 'package:flutter/foundation.dart';

const ROUTES = {
  "LOGIN": "/login",
  "REGISTER": "/register",
};

class User extends ChangeNotifier {
  bool _isLoggedIn;
  String _id;
  
  static User _instance;

  static User create() {
    _instance = new User(false);
    return _instance;
  }

  static User get instance {
    return _instance;
  }

  User(bool isLoggedIn) {
    this._isLoggedIn = isLoggedIn;
  }

  bool get isLoggedIn {
    //TODO(danck) actually check if user is loggedIn (persistent data (optimistic) or wait for API call (pessimistic)?)
    return this._isLoggedIn;
  }

  String get id {
    return this._id;
  }

  void register(String id, String pw) {
    this._id = id;
    var userData = {'fullname': '(HTTPConnector) DUMMY VALUE'};

    var body =
        json.encode({'username': id, 'password': pw, 'user_data': userData});

    Backend.post(ROUTES["REGISTER"], body);
  }

  void login(String id, String pw) {
    //TODO(danck) implement error handling
    var body = json.encode({'username': id, 'password': pw});
    Backend.post(ROUTES["LOGIN"], body);
    this._isLoggedIn = true;
    notifyListeners();
  }
}
