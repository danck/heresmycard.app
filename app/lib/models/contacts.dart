import 'package:flutter/foundation.dart';
import 'dart:convert';
import 'package:heresmycard_frontend/models/contact.dart';
import 'package:heresmycard_frontend/connectors/dummy_data.dart';
import 'package:heresmycard_frontend/models/user.dart';
import 'package:heresmycard_frontend/connectors/backend.dart';

const ROUTES = {
  // TODO(danck): Schema?
};

class Contacts extends ChangeNotifier {
  List<Contact> _contacts;

  static listFromJson(Map<String, dynamic> json){
    var contacts = json.values.map((v) => new Contact.fromJson(v)).toList();
    return contacts;
  }

  static Contacts create() {
    var contactList = (jsonDecode(DUMMY_CONTACTS) as List)
        .map((e) => new Contact.fromJson(e))
        .toList();
    return Contacts(contactList);
  }

  Contacts(this._contacts);

  void update() async {
    Backend.get("/api/user/" + User.instance.id+ "/data");
    notifyListeners();
  }

  List<Contact> getAll() {
    return this._contacts;
  }

  Contact getByIndex(int index) {
    return this._contacts[index];
  }

  void resetContactsWith(List<Contact> contacts) {
    this._contacts.clear();
    this._contacts.addAll(contacts);
    notifyListeners();
  }
}
