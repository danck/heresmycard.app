import 'package:flutter/material.dart';

class Contact extends ChangeNotifier {
  final String _timeConnected;
  final List<String> _tags;
  final ContactFieldsList _fields;

  Contact(this._timeConnected, this._tags, this._fields);

  factory Contact.fromJson(Map<String, dynamic> json) {
    String timeConnected = json['timeConnected'];
    List<String> tags = new List<String>.from(json['tags']);
    ContactFieldsList fields = new ContactFieldsList.fromJson(json['fields']);

    return new Contact(timeConnected, tags, fields);
  }

//TODO(danck): Change data model of Contact to like:
// { tags: []Tag, card: Card }
// and obtain fields, name and description from Card

  String get fullName {
    //TODO(danck): Add fullName to the data model as first order property
    return this._fields._fields.where((f) => f._description == "fullName").first._content;
  }

  String get picture {
    //TODO(danck): Add picture to the contact data model as first order property
    return "DUMMY PICTURE";
  }

  String get description {
    //TODO(danck): Add description to the contact data model as first order property
    return "DUMMY DESCRIPTION";
  }

  List<String> get tags {
    return this._tags;
  }

  List<ContactField> get fields {
    return this._fields._fields;
  }
}

enum ContactFieldType { WEBLINK, PHONE, ADDRESS, UNSPEFICIED }

class ContactField {
  String _description;
  String _content;

  ContactField(this._description, this._content);

  factory ContactField.fromJson(Map<String, dynamic> json) {
    return ContactField(json['key'], json['value']);
  }

  String get description {
    return this._description;
  }

  String get content {
    return this._content;
  }
}

class ContactFieldsList {
  final List<ContactField> _fields;

  ContactFieldsList(this._fields);

  factory ContactFieldsList.fromJson(List<dynamic> json) {
    List<ContactField> fields = new List<ContactField>();

    fields = json.map((i) => ContactField.fromJson(i)).toList();
    return ContactFieldsList(fields);
  }

  List<ContactField> get fields {
    return this._fields;
  }
}
