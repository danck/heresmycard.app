import 'package:flutter/material.dart';

class MyDetail extends ChangeNotifier {
  String _type;
  String _name;
  String _value;

  MyDetail({String type, String name, String value}) : super() {
    this._type = type;
    this._name = name;
    this._value = value;
    //TODO(danck): Notify repository
  }

  String get type {
    return this._type;
  }
  
  String get name {
    return this._name;
  }
  
  String get value {
    return this._value;
  }

  set type(String type) {
    this._type = type;
    notifyListeners();
    //TODO(danck): Notify repository
  }
  
  set name(String name) {
    this._name = name;
    notifyListeners();
    //TODO(danck): Notify repository
  }
  
  set value(String value) {
    this._value = value;
    notifyListeners();
    //TODO(danck): Notify repository
  }
}