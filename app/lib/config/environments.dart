import 'package:flutter/material.dart';

// This class provides parameters that depend on the deployment mode

enum Flavor {
  DEV,
  STAGING,
  PRODUCTION
}

class FlavorValues {
  FlavorValues({@required this.apiBaseUrl});
  final String apiBaseUrl;
}

class FlavorConfig {
  final Flavor flavor;
  final String name;
  final FlavorValues values;
  static FlavorConfig _instance;
  
  factory FlavorConfig({
      @required Flavor flavor,
      @required FlavorValues values}) {
    _instance ??= FlavorConfig._internal(
        flavor, flavor.toString(), values);
    return _instance;
  }
  
  FlavorConfig._internal(this.flavor, this.name, this.values);
  static FlavorConfig get instance { return _instance;}
  static bool isProduction() => _instance.flavor == Flavor.PRODUCTION;
  static bool isDevelopment() => _instance.flavor == Flavor.DEV;
  static bool isStaging() => _instance.flavor == Flavor.STAGING;
}