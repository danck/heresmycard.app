import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:heresmycard_frontend/config/environments.dart';

class Backend {
  static Session _instance;

  static Session get _session {
    if (_instance == null) {
      _instance = new Session();
    }

    return _instance;
  }

   static get(String ressource) {
    Backend._session.get(ressource).then((map) => map)
    .catchError((e) => throw(e)); //TODO(danck) implement error handling
  } 

  static post(String ressource, dynamic data) {
    Backend._session.post(ressource, data).then((map) => map)
    .catchError((e) => throw(e)); //TODO(danck) implement error handling
  }
}

class Session {
  final String ENDPOINT = FlavorConfig.instance.values.apiBaseUrl;
  var uri;
  Map<String, String> headers = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
  };

  Future<Map> get(String ressource) async {
    uri = ENDPOINT + ressource;
    http.Response response = await http.get(uri, headers: headers);
    updateHeaders(response);
    return json.decode(response.body);
  }

  Future<Map> post(String ressource, dynamic data) async {
    uri = ENDPOINT + ressource;
    http.Response response = await http.post(uri, body: data, headers: headers).catchError(() => {}); //TODO(danck): handle errors
    updateHeaders(response);
    if (response.bodyBytes.lengthInBytes == 0) {
      return null;
    }
    return json.decode(response.body);
  }

  void updateHeaders(http.Response response) {
    String rawCookie = response.headers['set-cookie'];

    if (rawCookie != null) {
      int index = rawCookie.indexOf('=');
      String name = rawCookie.substring(0, index);

      if (name=="heresmycardapp_access_token") {
        int index2 = rawCookie.indexOf(";");
        String token = rawCookie.substring(index + 1, index2);
        headers['Authorization'] = "Bearer " + token;
      }
    }

    print('State of headers ' + headers.toString());
  }
}
