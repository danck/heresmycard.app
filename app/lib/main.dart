import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:heresmycard_frontend/views/navigator_page.dart';
import 'package:heresmycard_frontend/views/contacts/contacts.dart';
import 'package:heresmycard_frontend/models/contacts.dart';
import 'package:heresmycard_frontend/views/connect/connect.dart';
import 'package:heresmycard_frontend/views/my_cards/my_cards.dart';
import 'package:heresmycard_frontend/models/user.dart';
import 'package:heresmycard_frontend/views/registration_screen.dart';
import 'package:heresmycard_frontend/config/environments.dart';

/*%%%%%%%
todo: refactor according to this
https://flutter.dev/docs/development/data-and-backend/state-mgmt/simple
https://github.com/flutter/samples/tree/master/provider_shopper/lib/common
%%%%%%%*/

void main() {
  FlavorConfig(flavor: Flavor.DEV, values: FlavorValues(apiBaseUrl: "http://192.168.56.101:5001"));

  runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider<User>(
          create: (context) => User.create(),
        ),
        ChangeNotifierProvider<Contacts>(create: (context) => Contacts.create())
      ],
      child: MaterialApp(
          title: "Here's My Card",
          theme: ThemeData(
              //TODO(danck) Define and implement app-wide color/text theme
              brightness: Brightness.light,
              backgroundColor: Colors.white,
              bottomAppBarColor: Colors.grey[800],
              textTheme: TextTheme()),
          darkTheme: ThemeData(
              brightness: Brightness.dark,
              backgroundColor: Colors.black87,
              textTheme: TextTheme()),
          home: Starter())));
}

class Starter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (Provider.of<User>(context).isLoggedIn) {
      return HMCHome();
    } else {
      return RegistrationScreen();
    }
  }
}

class HMCHome extends StatefulWidget {
  @override
  HMCHomeState createState() => HMCHomeState();
}

class HMCHomeState extends State<HMCHome> {
  int _pageIndex = 0;
  List<NavigatorPage> _children = <NavigatorPage>[
    NavigatorPage(child: ContactsTab()),
    NavigatorPage(child: NewConnectionTab()),
    NavigatorPage(child: MyCardsTab())
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: IndexedStack(
          index: _pageIndex,
          children: _children,
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Theme.of(context).bottomAppBarColor,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.contacts, color: Colors.grey),
              activeIcon: Icon(Icons.contacts, color: Colors.white),
              title: Text(
                'Contacts',
                style: TextStyle(color: Colors.white),
              )),
          BottomNavigationBarItem(
              icon: Icon(Icons.person_add, color: Colors.grey),
              activeIcon: Icon(Icons.person_add, color: Colors.white),
              title: Text('Connect', style: TextStyle(color: Colors.white))),
          BottomNavigationBarItem(
              icon: Icon(Icons.person_pin, color: Colors.grey),
              activeIcon: Icon(Icons.person_pin, color: Colors.white),
              title: Text('My Cards', style: TextStyle(color: Colors.white))),
        ],
        currentIndex: _pageIndex,
        onTap: (int index) {
          setState(() {
            _pageIndex = index;
          });
        },
      ),
    );
  }
}
