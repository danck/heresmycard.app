import 'package:flutter/material.dart';

class NavigatorPage extends StatefulWidget {
  const NavigatorPage({Key key, this.child}) : super(key: key);

  final Widget child;

  @override
  _NavigatorPageState createState() => _NavigatorPageState(child);
}

class _NavigatorPageState extends State<NavigatorPage> {
  Widget _child;

  _NavigatorPageState(Widget child) {
    _child = child;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context);
        return false;
      },
      child: Navigator(
        onGenerateRoute: (RouteSettings settings) {
          return new MaterialPageRoute(
            settings: settings,
            builder: (BuildContext context) {
              return _child;
            },
          );
        },
      ),
    );
  }
}
