import 'package:flutter/material.dart';
import 'package:heresmycard_frontend/models/contact.dart';

class ContactDetailScreen extends StatelessWidget {
  final Contact contact;

  ContactDetailScreen({Key key, @required this.contact}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Theme.of(context).backgroundColor,
        child: SingleChildScrollView(
            child: Column(
          children: <Widget>[
            _HeaderSection(key, contact),
            Divider(),
            _TagArea(key, contact),
            Divider(),
            _DetailsArea(key, contact)
          ],
        )));
  }
}

class _HeaderSection extends StatelessWidget {
  final Contact contact;

  _HeaderSection(Key key, this.contact) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[_Picture(key, contact), _Overview(key, contact)],
    );
  }
}

class _Picture extends StatelessWidget {
  final Contact contact;

  _Picture(Key key, this.contact) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(radius: 40.0, child: Text('${contact.picture}'));
  }
}

class _Overview extends StatelessWidget {
  final Contact contact;

  _Overview(Key key, this.contact) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text('${contact.description}');
  }
}

class _TagArea extends StatelessWidget {
  final Contact contact;

  _TagArea(Key key, this.contact) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
              child: Wrap(
                  children:
                      contact.tags.map((t) => Card(child:  Text(t))).toList())),
          MaterialButton(
            child: Icon(Icons.add_circle),
            onPressed: () => {}, //TODO(danck): Implement logic: Adding Tags
          )
        ]);
  }
}

///*
class _DetailsArea extends StatelessWidget {
  final Contact contact;

  _DetailsArea(Key key, this.contact) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var rows = contact.fields
        .map((f) => Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[Text(f.description), Text(f.content)])))
        .toList();

    return Column(children: rows);
  }
}
