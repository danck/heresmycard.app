import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:heresmycard_frontend/models/contact.dart';
import 'package:heresmycard_frontend/models/contacts.dart';
import 'package:heresmycard_frontend/views/contacts/contact_detail.dart';

class ContactsList extends StatefulWidget {
  ContactsList({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ContactsListState createState() => new _ContactsListState();
}

class _ContactsListState extends State<ContactsList> {
  TextEditingController editingController = TextEditingController();

  var contacts = List<Contact>();
  Contacts globalContacts;

  @override
  void initState() {
    var duplicateContacts =
        Provider.of<Contacts>(context, listen: false).getAll();
    contacts.addAll(duplicateContacts);
    super.initState();
  }

  void filterSearchResults(String query, List<Contact> duplicateContacts) {
    List<Contact> dummySearchList = List<Contact>();
    dummySearchList.addAll(duplicateContacts);
    if (query.isNotEmpty) {
      List<Contact> dummyListData = List<Contact>();
      dummySearchList.forEach((item) {
        if (item.fullName.contains(query)) {
          dummyListData.add(item);
        }
      });
      setState(() {
        contacts.clear();
        contacts.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        contacts.clear();
        contacts.addAll(duplicateContacts);
      });
    }
  }

  Future<String> _handleUpdate() async {
    globalContacts.update();
    return 'Done';
  }

  @override
  Widget build(BuildContext context) {
    globalContacts = Provider.of<Contacts>(context);
    var duplicateContacts = globalContacts.getAll();

    return new Scaffold(
      body: Container(
        child: RefreshIndicator(
          onRefresh: _handleUpdate,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  onChanged: (value) {
                    filterSearchResults(value, duplicateContacts);
                  },
                  controller: editingController,
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      labelText: "Search",
                      hintText: "Search",
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(32.0)))),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: contacts.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      leading: CircleAvatar(
                          child: Text(contacts[index].fullName[0])),
                      title: Text('${contacts[index].fullName}'),
                      subtitle: Text('${contacts[index].description}'),
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) =>
                                ContactDetailScreen(contact: contacts[index]),
                          ),
                        );
                      },
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
