import 'package:flutter/material.dart';
import 'package:heresmycard_frontend/views/contacts/contactslist.dart';

class ContactsTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ContactsList();
  }
}