import 'package:flutter/material.dart';

class MyCardsTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Heading
    // Detail 1
    // Detail 2
    // Detail 3
    // ...
    // Card 1 | Card 2 | Card 3 | ...

    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: new Container(
          child: new SingleChildScrollView(
              key: key,
              child: new Column(
                children: <Widget>[
                  Center(child: Text('My Details', style: Theme.of(context).textTheme.headline)),
                  MyDetailsList(key: key),
                  Center(child: Text('My Cards', style: Theme.of(context).textTheme.headline)),
                  MyCardsList(key: key)
                ],
              )),
        ));
  }
}

class MyDetailsList extends StatefulWidget {
  const MyDetailsList({Key key}) : super(key: key);

  @override
  _MyDetailsListState createState() => _MyDetailsListState();
}

class _MyDetailsListState extends State<MyDetailsList> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.map),
              title: Text('Map'),
            ),
            ListTile(
              leading: Icon(Icons.photo_album),
              title: Text('Album'),
            ),
            ListTile(
              leading: Icon(Icons.phone),
              title: Text('Phone'),
            ),
            ListTile(
              leading: Icon(Icons.map),
              title: Text('Map'),
            ),
            ListTile(
              leading: Icon(Icons.photo_album),
              title: Text('Album'),
            ),
            ListTile(
              leading: Icon(Icons.phone),
              title: Text('Phone'),
            ),
            ListTile(
              leading: Icon(Icons.map),
              title: Text('Map'),
            ),
            ListTile(
              leading: Icon(Icons.photo_album),
              title: Text('Album'),
            ),
            ListTile(
              leading: Icon(Icons.phone),
              title: Text('Phone'),
            ),
          ],
        ));
  }
}

class MyCardsList extends StatefulWidget {
  const MyCardsList({Key key}) : super(key: key);
  @override
  _MyCardsListState createState() => _MyCardsListState();
}

class _MyCardsListState extends State<MyCardsList> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 200.0,
      child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              Container(
                width: 160.0,
                color: Colors.red,
              ),
              Container(
                width: 160.0,
                color: Colors.blue,
              ),
              Container(
                width: 160.0,
                color: Colors.green,
              ),
              Container(
                width: 160.0,
                color: Colors.yellow,
              ),
              Container(
                width: 160.0,
                color: Colors.orange,
              ),
            ],
          )
    );
  }
}
