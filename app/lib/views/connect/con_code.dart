import 'package:flutter/material.dart';

class ConCodeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text("Let other people connect with this code"),
              Text(
                "VGVzdDEyMw==",
                textScaleFactor: 2.0,
              ),
              Divider(),
              Text("Enter a connection code here"),
              MyCustomForm(),
            ])));
  }
}

class MyCustomForm extends StatefulWidget {
  @override
  _MyCustomFormState createState() => _MyCustomFormState();
}

class _MyCustomFormState extends State<MyCustomForm> {
  final myController = TextEditingController();

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      TextField(
        controller: myController,
      ),
      FlatButton(
        color: Colors.grey[300],
        child: Text("Connect"),
        onPressed: () {
          return showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text(myController.text),
              );
            },
          );
        },
        //child: Icon(Icons.text_fields),
      ),
    ]);
  }
}
