import 'package:flutter/material.dart';
import 'package:heresmycard_frontend/views/connect/side_by_side.dart';
import 'package:heresmycard_frontend/views/connect/qr_code.dart';
import 'package:heresmycard_frontend/views/connect/con_code.dart';

class NewConnectionTab extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
        child: Center(
          child: Column(
            // center the children
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ConnectionType(
                  Icons.leak_add,
                  'Side by Side',
                  'Hold your devices side by side to connect',
                  SideBySideScreen()),
              Divider(),
              ConnectionType(Icons.crop_free, 'Scan QR Code',
                  'Scan or generate a QR Code to connect', QRCodeScreen()),
              Divider(),
              ConnectionType(
                  Icons.add_to_home_screen,
                  'Enter Connection Code',
                  'Use a code that you can copy or write down',
                  ConCodeScreen()),
            ],
          ),
        ),
      ),
    );
  }
}

class ConnectionType extends StatelessWidget {
  final IconData _icon;
  final String _title;
  final String _text;
  final Widget _screen;

  ConnectionType(this._icon, this._title, this._text, this._screen);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          this._doSome(context);
        },
        child: ListTile(
            leading: Icon(
              _icon,
              size: 55.0,
              color: Colors.grey[800],
            ),
            title: Text(
              _title,
              style: TextStyle(
                  color: Colors.grey[800],
                  fontWeight: FontWeight.w900,
                  fontStyle: FontStyle.normal,
                  fontSize: 20),
            ),
            subtitle: Text(
              _text,
              style: TextStyle(
                  color: Colors.grey[600],
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.normal,
                  fontSize: 14),
            )));
  }

  _doSome(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => _screen));
  }
}

