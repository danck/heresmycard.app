# Native App
The web frontend is implemented as a *Flutter App*.

## Stack
* Platform independent framework for mobile apps: [Flutter](https://flutter.dev/)

## Architecture
### Components

Simplified overview:
```plantuml
@startuml

package "Access Layer" {
  component [<<view>> \nConnections] as connections
  component [<<view>> \nMyCards] as myCards
  component [<<view>> \nConnect] as connect
}

package "Business Logic Layerc" {
  component [<<models>> \nUser] as user
  component [<<models>> \nCard] as card
  component [<<models>> \nCards] as cards
}

package "Data Layer " {
  component [<<connector>> \nlocalStorage] as storage
  component [<<connector>> \nbackend] as backend
}

database "LocalStorage" as localstorage {
}

interface "Backend Connector" as bconn
interface "Local Storage Connector" as lconn

cloud "Backend" as bcloud

localstorage -up- lconn
backend -up- bconn
connections -- card
user -down-( bconn
card -down-( bconn
cards -down-( bconn
user -down-( lconn
card -down-( lconn
cards -down-( lconn
myCards -- cards
storage -- localstorage
backend -down- bcloud


@enduml
```
