package handlers

import "net/http"

type userServices interface {
	AddUser(username string, fullname string, password string) ([]uint8, error)
	Authenticate(username string, password string) (bool, error)
	SetJWTiat(username string, iat int64) error
	GetUserData(username string) (*map[string]string, error)
	UpdateUserData(username string, userData *map[string]string) error
}

type middlewareServices interface {
	CreateJWT(username string) (*http.Cookie, int64, error)
}
