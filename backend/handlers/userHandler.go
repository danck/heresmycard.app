package handlers

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

type handler struct {
	userS userServices
	mwS   middlewareServices
}

type userBody struct {
	Username string            `json:"username"`
	Password string            `json:"password"`
	UserData map[string]string `json:"user_data"`
}

func CreateHandler(userS userServices, mwS middlewareServices) *handler {
	handler := new(handler)
	handler.userS = userS
	handler.mwS = mwS
	return handler
}

// decodeUserRequest decodes the body for a user specific request
func decodeUserRequest(r *http.Request) (userBody, error) {
	var data userBody
	if r.Body == nil {
		return data, errors.New("empty request body")
	}
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		return data, err
	}
	return data, nil
}

// CreateAccount creates a 'User' object from the request body and
// fowards it to the database for insertion.
//
// returns 400 if body is empty or not valid, or if username, password or fullname is not valid
// returns 409 if the given username is already in use
// returns 500 if the user could not be stored in the database
// returns 201 if the user is successfully created
func (h *handler) CreateAccount(w http.ResponseWriter, r *http.Request) {
	data, err := decodeUserRequest(r)
	if err != nil {
		log.Printf("CreateAccount: request body not valid")
		log.Printf(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	username := data.Username //TODO check for valid username
	password := data.Password //TODO check for valid password
	fullname := data.UserData["fullname"]
	if username == "" || password == "" || fullname == "" {
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("CreateAccount %s: username or password or fullname empty", username)
		return
	}
	res, err := h.userS.AddUser(username, fullname, password)
	if err != nil {
		if res == nil {
			log.Printf("CreateAccount %s: username already in use", username)
			w.WriteHeader(http.StatusConflict)
			return
		}
		log.Printf("CreateAccount %s: Error adding user to database", username)
		log.Printf(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
	log.Printf("CreateAccount %s: success", username)
	return
}

// Authenticate authenticates the given user and sets a JTW token cookie.
//
// returns 400 if body is empty or not valid, or if username, password is not valid
// returns 401 if username or password is incorrect
// returns 500 if the JWT token could not be generated or the database query failed
// returns 200 and cookie on succesful authentivication
func (h *handler) Authenticate(w http.ResponseWriter, r *http.Request) {
	data, err := decodeUserRequest(r)
	if err != nil {
		log.Printf("Authenticate: invalid request body")
		log.Printf(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	username := data.Username
	password := data.Password //TODO change to hmac
	if username == "" || password == "" {
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Authenticate %s: username or password empty", username)
		return
	}
	authenticated, err := h.userS.Authenticate(username, password)
	if err != nil {
		log.Printf("Authenticate %s: database error", username)
		log.Printf(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if authenticated {
		jwtCookie, jwtIAT, err := h.mwS.CreateJWT(username)
		if err != nil {
			log.Printf("Authenticate %s: error generating JwtToken", username)
			log.Printf(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		err = h.userS.SetJWTiat(username, jwtIAT)
		if err != nil {
			log.Printf("Authenticate %s: error setting JWT iat", username)
			log.Printf(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		http.SetCookie(w, jwtCookie)
		w.WriteHeader(http.StatusOK)
		log.Printf("jwtToken created: %s %s", jwtCookie.Name, jwtCookie.Value)
		log.Printf("Authenticate %s: success", username)
		return
	}
	// not authenticated
	w.WriteHeader(http.StatusBadRequest)
	log.Printf("Authenticate %s: authentification failed", username)
	return
}

// GetUserData returns the custom user data for a user
//
// returns 500 if the user data could not be found or validated
// returns 200 and json response on success
func (h *handler) GetUserData(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	username := params["username"]
	data, err := h.userS.GetUserData(username)
	if err != nil {
		log.Printf("GetUserData %s: error fetching user data", username)
		log.Printf(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	dataJSON, err := json.Marshal(*data)
	if err != nil {
		log.Printf("GetUserData %s: failed to encode user data", username)
		log.Printf(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(dataJSON)
	log.Printf("GetUserData %s: success", username)
	return
}

// SetUserData bulk updates the custom user data for a user
//
// returns 500 if the user data could not be saved
// returns 400 if the body cannot be validated or the fullname is missing
// returns 200 on success
func (h *handler) UpdateUserData(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	username := params["username"]
	data, err := decodeUserRequest(r)
	if err != nil {
		log.Printf("UpdateUserData %s: invalid request body", username)
		log.Printf(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	err = h.userS.UpdateUserData(username, &data.UserData)
	if err != nil {
		if strings.Contains(err.Error(), "fullname required") {
			log.Printf("UpdateUserData %s: fullname missing", username)
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		log.Printf("UpdateUserData %s: error saving user data", username)
		log.Printf(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Printf("UpdateUserData %s: success", username)
	w.WriteHeader(http.StatusOK)
	return
}

/*
//TODO update func
func DeleteAccount(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	username := params["username"]
	password := r.FormValue("password")
	if username == "" || password == "" {
		w.WriteHeader(http.StatusUnauthorized)
		log.Printf("DeleteAccount %s: username or password empty", username)
		return
	}
	rows := models.DBCon.DeleteUser(username, password)
	if rows == 1 {
		w.WriteHeader(http.StatusOK)
		log.Printf("DeleteAccount %s: success", username)
		return
	}
	w.WriteHeader(http.StatusInternalServerError)
	log.Printf("DeleteAccount %s: username or password wrong", username)
	return
}

//TODO update func
func UpdatePassword(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	oldPassword := r.FormValue("password")
	newPassword := r.FormValue("newPassword")
	if username == "" || oldPassword == "" || newPassword == "" {
		w.WriteHeader(http.StatusUnauthorized)
		log.Printf("UpdatePassword %s: username or password empty", username)
		return
	}
	rows, err := models.DBCon.Authenticate(username, oldPassword) //TODO check if necessery
	if err != nil {
		log.Printf("UpdatePassword %s: authentication error", username)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	if rows == 1 {
		if models.DBCon.UpdatePassword(username, oldPassword, newPassword) == 1 {
			log.Printf("UpdatePassword %s: success", username)
			w.WriteHeader(http.StatusOK)
			return
		}
		log.Printf("UpdatePassword %s: user not found", username)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusUnauthorized)
	log.Printf("UpdatePassword %s: username or password wrong", username)
	return
}
*/
