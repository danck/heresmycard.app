# Backend
The backend provides an endpoint for the REST API that allows all frontend apps (web, IOS and Android) to make requests to the database, handle authentication and create new content. 

## Stack
* Go Programming Language [Golang](https://golang.org/)
* Database: [PostgreSQL](https://www.postgresql.org/)

## API Doc
### Contacts

| Verb | URL | Request Body (example) | Response Body (example) | Description
| ---      |  ------  | --- | --------- | ---- |
| `GET` | `/api/user/:id/contact/:id` |  |  {<br/>"id":"1",<br/>"fullname":"example",<br/>"tags":["example","example"],<br/>"details":[{...},...]<br/>} |
| `GET`   | `/api/user/:id/contacts`   |   | [{contact},{contact},{contact},...] |

### Cards

| Verb | URL | Request Body (example) | Response Body (example) | Description
| ---      |  ------  | --- | --------- | --- |
| `GET` | `/api/user/:id/card/:id` |  | {<br/>"id":"1",<br/>"name":"example",<br/>"details":[{...},...]<br/>} | Retrieve existing card |
| `GET`   | `/api/user/:id/cards`   |   | [{card},{card},{card},...] | Retrieve all cards of user |
| `POST`   | `/api/user/:id/card`   | {<br/>"name":"example",<br/>"details":[{...},...]<br/>}  | {<br/>"id":"1",<br/>"name":"example",<br/>"details":[{...},...]<br/>} | Create a new card |
| `POST`   | `/api/user/:id/card/:id`   | {<br/>"name":"example",<br/>"details":[{...},...]<br/>} | | Update an existing card |


## Architecture
### Components
```plantuml
@startuml

package "Access Layer" {
  component [<<handlers>> \nuserHandler] as userHandler
  component [<<handlers>> \ncardHandler] as cardHandler
  component [<<middleware>> \njwtAuth] as jwtAuth
}

package "Business Logic" {
  component [<<models>> \nuser] as userModel
  component [<<models>> \ncards] as cardsModel
}

package "Data Layer" {
  component [<<repositories>> \nuserRepository] as userRepo
  component [<<repositories>> \ncardRepo] as cardRepo
  component [<<repositories>> \ndb] as dbRepo
}

interface "userServices" as US
interface "userServicesAuth" as USA
interface "cardServices" as CS

interface "userRepository" as UR
interface "cardRepository" as CR

userModel -up- US
userModel -up- USA
cardsModel -up- CS

userRepo -up- UR
cardRepo -up- CR

userHandler .left.> jwtAuth : use 
userRepo ..> dbRepo : use
cardRepo ..> dbRepo : use


jwtAuth -down-( USA
userHandler -down-( US
cardHandler -down-( CS

userModel --( UR
cardsModel --( CR



@enduml
```

### Data Model

```plantuml

' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "Users" as users {
  *user_id : number <<generated>>
  --
  username : text
  password : text
  role : text
  disabled : boolean
  deleted : boolean
  created_on : timestamp
  last_login : timestamp
  language : text
  verified : boolean
  user_data  : jsonb
  jwt_iat : bigint
}

entity "User Details" as details {
    *user_detail_id : UUID
    --
    *user_id : uuid <<FK>>
    detail_type : text,
    detail_name : text
    content : JSONB   
}

entity "User Details on Cards" as details_cards_relations {
    *card_id : UUID <<FK>>,
    *user_detail_id : UUID <<FK>>
}

entity "Cards" as cards {
    *card_id : uuid <<PK>> <<FK>>
    --
    *user_id : uuid <<FK>>
    name : text
    created_on : timestamp
}

entity "Users Contacts" as users_contacts {
    *user_id : uuid <<PK>> <<FK>>
    --
    *card_id : uuid <<FK>>
    tags : text[],
    created_on : timestamp,
    alias : text
}

details }o-- users
cards }o-- users
details_cards_relations }o-- cards
details_cards_relations }o-- details
users_contacts }o-- users
cards }o-- users_contacts

```

## Development
### Prerequisites
* Golang Version >1.13

### Deploy for local development
```
$ sudo docker-compose up
```
To reload changes of the db configuration it can become necessary to tear down previous builds first with
```
$ sudo docker-compose down
```

