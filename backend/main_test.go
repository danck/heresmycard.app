package main

import "testing"

func testMainPanic(t *testing.T) {
	defer func() {
        	if r := recover(); r != nil {
        }
    	}()

    	main()

    	t.Errorf("Expected PANIC when run without configuration")
}
