package models

import (
	"errors"
	"time"
)

type userService struct {
	repo userRepository
}

//TODO clean up
type user struct {
	username  string
	fullname  string            `json:",omitempty"`
	role      string            `json:",omitempty"`
	disabled  bool              `json:",omitempty"`
	deleted   bool              `json:",omitempty"`
	createdOn time.Time         `json:"created_on,omitempty" sql:"created_on"`
	lastLogin time.Time         `json:"last_login,omitempty" sql:"last_login"`
	language  string            `json:",omitempty"`
	verified  bool              `json:",omitempty"`
	userData  map[string]string `json:"user_data,omitempty" sql:"user_data"`
}

// helper func
func createNewUser(username string, fullname string) *user {
	var u = new(user)
	u.username = username
	u.userData = make(map[string]string)
	u.userData["fullname"] = fullname
	u.role = "user"
	u.createdOn = time.Now() //timezones?
	return u
}

// helper func
func (u *user) saveNewUser(repo userRepository, password string) ([]uint8, error) {
	res, err := repo.CreateNewUser(u.username, password,
		u.createdOn, u.role, u.userData)
	return res, err
}

// CreateUserService creates a service for the handlers. Depends on repository layer.
func CreateUserService(userRepo userRepository) *userService {
	uS := new(userService)
	uS.repo = userRepo
	return uS
}

// AddUser created a new user and forwards it to the repository for persistence.
// Returns user_id on success, nil if username already in use
func (uS *userService) AddUser(username string, fullname string, password string) ([]uint8, error) {
	if username == "" || fullname == "" || password == "" {
		return nil, errors.New("addUser: invalid params")
	}
	user := createNewUser(username, fullname)
	return user.saveNewUser(uS.repo, password)
}

// SetJWTiat sets the JWT iat timestamp for a user
func (uS *userService) SetJWTiat(username string, jwtIat int64) error {
	if username == "" || jwtIat == 0 {
		return errors.New("SetJWTiat: invalid params")
	}
	err := uS.repo.SetJWTiat(username, jwtIat)
	return err
}

// GetJWTiat gets the JWT iat timestamp for a user
func (uS *userService) GetJWTiat(username string) (int64, error) {
	if username == "" {
		return 0, errors.New("GetJWTiat: invalid params")
	}
	return uS.repo.GetJWTiat(username)

}

// Authenticate checks if the username and password are correct, as well as
// checking the disabled and deleted flag
func (uS *userService) Authenticate(username string, password string) (bool, error) {
	// TODO we're only forwarding username and password to data layer
	// and not creating an actual user object (necessary?)
	authenticated := false
	if username == "" || password == "" {
		return authenticated, errors.New("Authenticate: invalid params")
	}
	authenticated, err := uS.repo.Authenticate(username, password)
	return authenticated, err
}

// GetUserData fetches the user data from the user repo
func (uS *userService) GetUserData(username string) (*map[string]string, error) {
	data := new(map[string]string)
	if username == "" {
		return data, errors.New("GetUserData: invalid params")
	}
	data, err := uS.repo.GetUserData(username)
	return data, err
}

// UpdateUserData bulk updated the custom user data
func (uS *userService) UpdateUserData(username string, userData *map[string]string) error {
	if (*userData)["fullname"] == "" {
		return errors.New("fullname required")
	}
	return uS.repo.UpdateUserData(username, userData)
}

/*

func (db *DB) DeleteUser(username string, password string) int64 {
	sqlStatement := `
	DELETE FROM users WHERE users.username = $1 AND users.password = crypt($2,password)
	`
	res, err := db.Exec(sqlStatement, username, password)
	if err != nil {
		panic(err)
	}
	count, err := res.RowsAffected()
	if err != nil {
		panic(err)
	}
	return count
}

func (db *DB) UpdatePassword(username string, oldPassword string, newPassword string) int64 {
	sqlStatement := `
	UPDATE users SET password=crypt($3,gen_salt('bf'))
	WHERE username=$1 AND password=crypt($2,password)
	`
	res, err := db.Exec(sqlStatement, username, oldPassword, newPassword)
	if err != nil {
		panic(err)
	}
	count, err := res.RowsAffected()
	if err != nil {
		panic(err)
	}
	return count
}
*/
