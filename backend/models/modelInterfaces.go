package models

import "time"

type userRepository interface {
	CreateNewUser(username string, password string,
		createdOn time.Time, role string, userData map[string]string) ([]uint8, error)
	Authenticate(username string, password string) (bool, error)
	SetJWTiat(username string, jwtIat int64) error
	GetJWTiat(username string) (int64, error)
	GetUserData(username string) (*map[string]string, error)
	UpdateUserData(username string, userData *map[string]string) error
}

type cardRepository interface {
}
