package models

import (
	"time"
)

type card struct {
	cardID    int
	fields    []string
	createdOn time.Time
	name      string
}
