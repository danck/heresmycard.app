package repositories

type cardRepo struct {
	db *db
}

func CreateCardRepo(db *db) *cardRepo {
	repo := new(cardRepo)
	repo.db = db
	return repo
}
