package repositories

import (
	"database/sql"
	"fmt"
	"os"

	pq "github.com/lib/pq"
)

type db struct {
	*sql.DB
}

const (
	dbhost = "DBHOST"
	dbport = "DBPORT"
	dbuser = "DBUSER"
	dbpass = "DBPASS"
	dbname = "DBNAME"
)

// InitDB returns a databse connection
func InitDB() (*db, error) {
	config := dbConfig()
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		config[dbhost], config[dbport], config[dbuser],
		config[dbpass], config[dbname])
	database, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, err
	}
	if err = database.Ping(); err != nil {
		return nil, err
	}
	return &db{database}, nil
}

// dbConfig composes the database settings from os.envs
func dbConfig() map[string]string {
	var conf = make(map[string]string)
	host, ok := os.LookupEnv(dbhost)
	if !ok || host == "" {
		panic("DBHOST env not set")
	}
	port, ok := os.LookupEnv(dbport)
	if !ok || port == "" {
		panic("DBPORT env not set")
	}
	user, ok := os.LookupEnv(dbuser)
	if !ok || dbuser == "" {
		panic("DBUSER env not set")
	}
	pass, ok := os.LookupEnv(dbpass)
	if !ok || pass == "" {
		panic("DBPASS env not set")
	}
	name, ok := os.LookupEnv(dbname)
	if !ok || name == "" {
		panic("DBNAME env not set")
	}
	conf[dbhost] = host
	conf[dbport] = port
	conf[dbuser] = user
	conf[dbpass] = pass
	conf[dbname] = name
	return conf
}

func (db *db) getDBErrorCode(err error) (error, string) {
	if pgErr, ok := err.(*pq.Error); ok {
		return err, string(pgErr.Code)
	}
	return err, ""
}
