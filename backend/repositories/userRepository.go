package repositories

import (
	"encoding/json"
	"errors"
	"time"
)

type userRepo struct {
	db *db
}

// CreateUserRepo creates a user reposiotry instance
func CreateUserRepo(db *db) *userRepo {
	repo := new(userRepo)
	repo.db = db
	return repo
}

// CreateUser creates a new user in the database. Retruns the user_id on success,
// nil if the username is already in use
func (r *userRepo) CreateNewUser(username string, password string,
	createdOn time.Time, role string, userData map[string]string) ([]uint8, error) {
	//TODO check params?
	var id []uint8
	userDataJSON, err := json.Marshal(userData)
	if err != nil {
		return id, err
	}
	sqlStatement := `
		INSERT INTO users (username, password, created_on, role, user_data)
		VALUES ($1, crypt($2, gen_salt('bf')), $3, $4, $5)
		RETURNING user_id
		`
	err = r.db.QueryRow(sqlStatement, username, password, createdOn, role, userDataJSON).
		Scan(&id)
	if err != nil {
		pgErr, code := r.db.getDBErrorCode(err)
		if pgErr != nil {
			if code == "23505" {
				return nil, err
			}
		}
		return id, err
	}
	return id, nil
}

// Authenticate checks the given credentials and returns true if they are correct
// and the account is not disabled or deleted
func (r *userRepo) Authenticate(username string, password string) (bool, error) {
	//TODO check params?
	sqlStatement := `
	SELECT * FROM users WHERE username=$1 AND password=crypt($2,password)
	AND disabled=false AND deleted=false
	`
	res, err := r.db.Exec(sqlStatement, username, password)
	if err != nil {
		return false, err
	}
	rows, err := res.RowsAffected()
	if err != nil {
		return false, err
	}
	if rows == 1 {
		return true, nil
	}
	return false, err
}

// SetJWTiat sets the jwt_iat field for a user. Can be used to revoke token authorization
func (r *userRepo) SetJWTiat(username string, jwtIat int64) error {
	//TODO check params?
	sqlStatement := `
	UPDATE users SET jwt_iat=$2 WHERE username=$1
	`
	res, err := r.db.Exec(sqlStatement, username, jwtIat)
	if err != nil {
		return err
	}
	rows, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if rows == 1 {
		return nil
	}
	return errors.New("No matching database entry found")
}

// GetJWTiat gets jwt_iat field for a user.
func (r *userRepo) GetJWTiat(username string) (int64, error) {
	//TODO check params?
	var jwtIat int64
	sqlStatement := `
	SELECT jwt_iat FROM users WHERE username=$1
	`
	row := r.db.QueryRow(sqlStatement, username)
	err := row.Scan(&jwtIat)
	if err != nil {
		return -1, err
	}
	return jwtIat, nil
}

// GetUserData fetches the userdata from the database and returns it as a map[string]string
func (r *userRepo) GetUserData(username string) (*map[string]string, error) {
	jsonData := new([]byte)
	data := make(map[string]string)
	sqlStatement := `
	SELECT user_data FROM users WHERE username=$1
	`
	row := r.db.QueryRow(sqlStatement, username)
	err := row.Scan(&jsonData)
	if err != nil {
		return &data, err
	}
	err = json.Unmarshal(*jsonData, &data)
	if err != nil {
		return &data, err
	}
	return &data, nil
}

// SetUserData bulk updates the data for the user
func (r *userRepo) UpdateUserData(username string, userData *map[string]string) error {
	userDataJSON, err := json.Marshal(userData)
	if err != nil {
		return err
	}
	sqlStatement := `
	UPDATE users SET user_data = $2 WHERE username=$1
	`
	res, err := r.db.Exec(sqlStatement, username, userDataJSON)
	rows, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if rows == 1 {
		return nil
	}
	return errors.New("No matching database entry found")
}
