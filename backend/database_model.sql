/*DEV ONLY
script for initializing the database model
sudo su - postgres
psql -f [path to this file]
*/

drop database if exists heresmycarddb;
create database heresmycarddb;
grant all privileges on database heresmycarddb to heresmycard;

\c heresmycarddb;
CREATE EXTENSION if not exists "pgcrypto";
create extension if not exists "uuid-ossp";
grant all privileges on all tables in schema public to heresmycard;
set role heresmycard;


CREATE TABLE users ( 
	user_id UUID not null default uuid_generate_v4() primary key,
	username TEXT NOT NULL UNIQUE,
	password TEXT NOT NULL,
	role TEXT NOT NULL,
	disabled BOOLEAN NOT NULL default false,
	deleted boolean not null default false,
	created_on timestamp,
	last_login timestamp,
	language text,
	verified boolean default false,
	user_data jsonb not null,
	jwt_iat bigint not null default -1
);

create table user_details (
	user_detail_id UUID not null default uuid_generate_v4() primary key,
	user_id uuid references users(user_id),
	detail_type text,
	detail_name text NOT NULL,
	content JSONB NOT NULL
);

create table cards (
	card_id uuid not null default uuid_generate_v4() primary key,
	user_id uuid references users(user_id),
	name text,
	created_on timestamp
);

create table details_on_cards (
	card_id UUID references cards(card_id) primary key,
	user_detail_id UUID references user_details(user_detail_id)
);

create table users_contacts (
	user_id uuid references users(user_id) primary key,
	card_id uuid references cards(card_id),
	tags text[],
	created_on timestamp,
	alias text
);
