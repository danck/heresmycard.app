module gitlab.com/danck/heresmycard.app/backend

require (
	github.com/gorilla/mux v1.6.2

	heresmycard.app/backend/handlers v0.0.0
	heresmycard.app/backend/middleware v0.0.0
	heresmycard.app/backend/models v0.0.0
	heresmycard.app/backend/repositories v0.0.0
)

replace heresmycard.app/backend/handlers => ./handlers

replace heresmycard.app/backend/middleware => ./middleware

replace heresmycard.app/backend/models => ./models

replace heresmycard.app/backend/repositories => ./repositories

go 1.13
