package middleware

type userServices interface {
	GetJWTiat(string) (int64, error)
	SetJWTiat(username string, iat int64) error
}
