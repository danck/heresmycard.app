package middleware

import (
	"log"
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
)

type jwtClaims struct {
	Username string
	jwt.StandardClaims
}

type jwtMiddleware struct {
	userS  userServices
	jwtKey []byte
}

// CreateJWTMiddleware configures the JWT middleware. Depends on user service
// TODO fix key
func CreateJWTMiddleware(uS userServices, jwtKey []byte) *jwtMiddleware {
	jwtMiddleware := new(jwtMiddleware)
	jwtMiddleware.userS = uS
	jwtMiddleware.jwtKey = jwtKey
	return jwtMiddleware
}

func (jwtMW *jwtMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("Authorization")
		if authorizationHeader == "" {
			log.Printf("Authorization header not set")
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		params := mux.Vars(r)
		username := params["username"]
		tokenString := strings.Split(authorizationHeader, " ")[1]
		claims := &jwtClaims{}
		token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
			return jwtMW.jwtKey, nil
		})
		if !token.Valid {
			w.WriteHeader(http.StatusUnauthorized)
			log.Printf("Access token invalid")
			return
		}
		if err != nil {
			if err == jwt.ErrSignatureInvalid {
				log.Printf("Access token signature invalid")
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		// verify username
		if username != claims.Username {
			w.WriteHeader(http.StatusUnauthorized)
			log.Printf("Access token not valid for URL")
			return
		}
		// verify valid JWT iat
		jwtIat, err := jwtMW.userS.GetJWTiat(username)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("Error getting JWT iat")
			return
		}
		if jwtIat != claims.IssuedAt {
			w.WriteHeader(http.StatusUnauthorized)
			log.Printf("Access token iat not valid")
			return
		}
		next.ServeHTTP(w, r)
	})
}

func (jwtMW *jwtMiddleware) CreateJWT(username string) (*http.Cookie, int64, error) {
	iat := int64(time.Now().Unix())
	expirationTime := time.Now().Add(10 * time.Minute)
	claims := jwtClaims{
		Username: username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
			IssuedAt:  iat,
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtMW.jwtKey)
	if err != nil {
		return nil, 0, err
	}
	return &http.Cookie{
		Name:     "heresmycardapp_access_token",
		Value:    tokenString,
		Expires:  expirationTime,
		Secure:   true,
		HttpOnly: true,
	}, iat, nil
}
