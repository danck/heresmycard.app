package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"heresmycard.app/backend/handlers"
	"heresmycard.app/backend/middleware"
	"heresmycard.app/backend/models"
	"heresmycard.app/backend/repositories"
)

func main() {
	db, err := repositories.InitDB()
	if err != nil { // TODO(danck): Replace this with a better retry pattern
		log.Printf("Failed to initialize database, retrying: %s", err.Error())
		time.Sleep(3 * time.Second)
	}
	db, err = repositories.InitDB()
	if err != nil {
		log.Panic(err)
	}
	defer db.Close()
	log.Printf("Connected to database")
	userRepo := repositories.CreateUserRepo(db)
	userService := models.CreateUserService(userRepo)
	jwtKey := []byte("jwt secret key")
	middlewareService := middleware.CreateJWTMiddleware(userService, jwtKey)
	handler := handlers.CreateHandler(userService, middlewareService)

	router := mux.NewRouter()
	//use subrouter for authorized data access
	apiRouter := router.PathPrefix("/api").Subrouter()

	// register middleware
	apiRouter.Use(middlewareService.Middleware)

	// register controllers
	router.HandleFunc("/register", handler.CreateAccount).Methods("POST")
	router.HandleFunc("/login", handler.Authenticate).Methods("POST")
	apiRouter.HandleFunc("/user/{username}/data", handler.GetUserData).Methods("GET")
	apiRouter.HandleFunc("/user/{username}/data", handler.UpdateUserData).
		Methods("POST").Headers("Content-Type", "application/json")
	// apiRouter.HandleFunc("/user/{username}/delete", handlers.DeleteAccount).Methods("POST")
	// apiRouter.HandleFunc("/user/{username}/password", handlers.UpdatePassword).Methods("POST")

	//router.NotFoundHandler = controllers.NotFound

	port := os.Getenv("PORT")
	if port == "" {
		port = "5001"
	}

	log.Printf("Listening on port %s", port)
	err = http.ListenAndServe(":"+port, router)
	if err != nil {
		log.Fatal(err)
	}
}
