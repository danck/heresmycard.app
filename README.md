# Here's my Card APP

This repository contains all technical artefacts that belong to the project *Here's my Card APP*:
* Source Code for each component
* Scripts to build, deploy and manage any component
* Configuration files for databases and operating systems

It **must not** contain:
* Secret credentials

It **should not** contain:
* Binary builds of any of the components
* Legal documents
* Strategic and organisational documents

## App Impressions
![alt text](Screenshot1.png "List of contacts")
![alt text](Screenshot2.png "Contact details")
![alt text](Screenshot3.png "Own details and cards")

## Deployment

The system consists of mobile apps that connect to database over a RESTful endpoint.
The delivery of the frontend apps depend on the target platform.

```plantuml
@startuml

node "Android Device" {
    component "Android App" as android
}
node "IOS Device" {
    component "IOS App" as ios
}
node "Web Browser" {
    component "Web App" as webapp
}

node "CDN" {
    component "Web Server" as webserver
}

rectangle Backend {



node "API Server" <<VM>> {
    component Backend as api
}

node "DB Server" <<VM>> {
    database Postgres as db
}

}

android -- api
ios -- api : http
webapp -- api

webapp -- webserver : http

api -- db : label1


@enduml
```

## How do Users Connect?

Every user manages their details by assigning them to `card`s.
A connection is made by exchanging cards between two users.
During this exchange *references* to the other users `card` are made, each wrapped as a `contact`. No user data is copied.

```mermaid
sequenceDiagram
    Alice->>Backend: I want to offer my [card1]
    activate Backend
    note right of Backend: Backend:<br/>Makes unique [code] <br/>that maps to [card1]
    Backend-->>Alice: Here's the [code]
    deactivate Backend
    Alice-->>Bob: Hi Bob, have this [code]
    Bob->>Backend: Connect my [card2] with this [code] 
    activate Backend
    note left of Backend: Backend:<br/>Looks up [code]<br/>and gets [card1]. <br/>Then adds <br/>Contact[card2] to<br/>Alice and<br/>Contact[card1] to<br/>Bob
    Backend->>Bob: OK
    deactivate Backend
    
```

The `code` that is getting exchanged can be a QR image, an NFC tag or just a literal Base64 string.


## Deploy and Test
To trigger the continuous deployment pipeline set up this repo and push:

```mermaid
graph TD;
  start((push))-->testapp[Component tests: native apps]
  start-->testbackend[Component tests: backend]
  testapp-->integration[Integration tests]
  testbackend-->integration
  integration-->deplApp[Release-build native apps and deploy to staging]
  integration-->deplBack[Release-build backend and deploy to staging]
```
