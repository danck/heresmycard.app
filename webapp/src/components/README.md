# Components
Every page is composed out of components. 
A component can contain
* A html structure
* Custom styles
* Java script functions
* Other components