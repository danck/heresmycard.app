import Vue from 'vue'
import App from './App.vue'
import { router } from './router' // uses index.js by default
import store from './store' // uses index.js by default
import Scrollspy from 'vue2-scrollspy'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../assets/app.css'

Vue.use(Scrollspy)

/* eslint-disable-next-line no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
