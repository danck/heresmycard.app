// Testing
//export const DEBUG = true

// Backend API endpoints
export const TEST_ENDPOINT = 'http://192.168.56.101:5001'
export const API_BASE_URL = '/api'
export const API_USER_LOGIN_URL = '/login'
//export const REFRESH_TOKEN_URL = '/auth'

/**
* Key for local storage.
*
* Set the key to use in local storage to hold persistant data. If logged in,
* you can see this key by going to Chrome > dev tools > application tab,
* then choosing "Local Storage" and "http://localhost:8080".
*
* @type {string}
*/
export const STORAGE_KEY = 'heres-my-card-app'

