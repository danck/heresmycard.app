import Vue from 'vue'
import Vuex from 'vuex'
import Auth from './auth.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cards: [
      { id: 1, firstname: 'Firstname', lastname: 'Lastname' },
      { id: 2, firstname: 'Firstname', lastname: 'Lastname' },
      { id: 3, firstname: 'Firstname', lastname: 'Lastname' },
      { id: 4, firstname: 'Firstname', lastname: 'Lastname' }
    ] 
  },

  mutations: {},
  actions: {
    login (context, credentials) { 
      Auth.login(credentials) 
    }
  }
})
