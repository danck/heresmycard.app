# Store
The store holds the state of the entire application. 

Any component that contains or uses stateful information such as
* user name
* authenticaton state
* contact data
must address only the store for any operations on global data.