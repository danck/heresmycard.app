import { TEST_ENDPOINT, API_BASE_URL, API_USER_LOGIN_URL } from '../constants/constants.js'

const loginEndpoint = TEST_ENDPOINT + API_USER_LOGIN_URL

export default {
  login(credentials) {
    let url = loginEndpoint
    // Default options are marked with *
      return fetch(url, {
          method: "POST", // *GET, POST, PUT, DELETE, etc.
          mode: "no-cors", // no-cors, cors, *same-origin
          cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
          credentials: "same-origin", // include, *same-origin, omit
          headers: {
              "Content-Type": "application/json",
              // "Content-Type": "application/x-www-form-urlencoded",
          },
          redirect: "follow", // manual, *follow, error
          referrer: "no-referrer", // no-referrer, *client
          body: JSON.stringify(credentials), // body data type must match "Content-Type" header
      })
      .then(response => console.log(response)); // parses response to JSON
  }
}
