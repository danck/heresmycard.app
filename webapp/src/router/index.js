import Vue from 'vue'
import Router from 'vue-router'
import MyCollection from '../components/MyCollection.vue'
import MySettings from '../components/MySettings.vue'
import Login from '../pages/LoginPage.vue'

Vue.use(Router)

export const router = new Router ({
  routes: [
    {
      path: '/',
      name: 'MyCollection',
      component: MyCollection
    },
    {
      path: '/settings',
      name: 'MySettings',
      component: MySettings
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },

    // otherwise redirect to root
    {
      path: '*',
      redirect: '/'
    }
  ]
})

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  
  const publicPages = ['/login']
  const authRequired = !publicPages.includes(to.path) 
  const loggedIn = localStorage.getItem('user')

  if (authRequired && !loggedIn) {
    return next({
      path: '/login',
      query: { returnUrl: to.path }
    })
  }
  next()
})
