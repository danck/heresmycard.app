# Web App
The web frontend is implemented as a *Progressive Web App* (PWA).

## Stack
* Java Script Framework for Single Page Applications: [VueJS](https://vuejs.org/)
* Bundler to manage build and test: [Webpack](https://webpack.js.org/)

## Development
### Prerequisites
TODO
### Installation
TODO
### Test and Commit
TODO

## Deployment
### Run locally
Install dependencies if necessary and run on a local dev server
~~~~
npm install --save
npm run dev
~~~~

Alternatively use docker to start a containerized dev server
~~~~
./run.sh
~~~~

### Deploy to production
TODO

## Further Documentation
TODO
