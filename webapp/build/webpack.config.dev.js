// webpack.config.js
'use strict'

const webpack = require('webpack')
const VueLoaderPlugin  = require('vue-loader/lib/plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const Path = require('path')

function resolve(dir) {
  return Path.join(__dirname, '..', dir)
}

module.exports = {
  mode: 'development',
  entry : [
    './src/index.js'
  ],
  devServer: {
    host: '0.0.0.0',
    hot: true,
    watchOptions: {
        poll: true
    }
    },
      module: {
	      rules: [
		      // ... other rules
		      {
		          test: /\.vue$/,
		          loader: 'vue-loader'
          },
          {
            test: /\.(scss)$/,
            use: [{
              loader: 'style-loader', // inject CSS to page
            }, {
              loader: 'css-loader', // translates CSS into CommonJS modules
            }, {
              loader: 'postcss-loader', // Run post css actions
              options: {
                plugins: function () { // post css plugins, can be exported to postcss.config.js
                  return [
                     require('precss'),
                     require('autoprefixer')
                  ];
                }
              }
            }, {
              loader: 'sass-loader' // compiles Sass to CSS
            }]
          },
          {
            test: /\.css$/,
              use: [
                'vue-style-loader',
                {loader: 'css-loader'}
              ]
          }
        ]
		  },
      plugins: [
        // make sure to include the plugin!
        new webpack.HotModuleReplacementPlugin(),
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
          filename: 'index.html',
          template: 'index.html',
          inject: true
        }),
        new CopyWebpackPlugin([{
          from: resolve('static/images'),
          to: resolve('dist/static/images'),
          toType: 'dir'
        }])
		  ],
      externals: {
        // global app config object
        config: JSON.stringify({
          apiUrl: 'http://192.168.56.101:5001'
        })
}
}
